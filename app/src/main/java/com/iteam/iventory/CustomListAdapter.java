package com.iteam.iventory;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iteam.iventory.models.Item;

import java.util.ArrayList;

/**
 * Created by FQuilloy on 11/25/2017.
 */

public class CustomListAdapter extends ArrayAdapter {
    //to reference the Activity
    private final Activity context;

    //to store the list of countries
    private final ArrayList<Item> itemArray;

    public CustomListAdapter(Activity context, ArrayList<Item> items) {

        super(context, R.layout.inventory_list_row, items);

        this.context = context;
        this.itemArray = items;

    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.inventory_list_row, null, true);

        //this code gets references to objects in the listview_row.xml file
        TextView nameTextField = rowView.findViewById(R.id.nameTextView);
        TextView infoTextField = rowView.findViewById(R.id.countTextView);

        //this code sets the values of the objects to values from the arrays
        nameTextField.setText(itemArray.get(position).getName());
        infoTextField.setText(itemArray.get(position).getCount());


        return rowView;

    }
}