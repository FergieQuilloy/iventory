package com.iteam.iventory.manager;

import android.util.Log;

import com.iteam.iventory.models.Item;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by christinealcachupas on 25/11/2017.
 */

public class API {
    public static final API ourInstance = new API();
    public ArrayList<Item> itemArrayList;
    public Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://dev-iventory.pantheonsite.io/wp-json/iventory/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    static API getInstance() {
        return ourInstance;
    }

    private API() {
    }

    public interface IventoryService {
        @GET("get-records")
        Call<List<Item>> getItems();

        @POST("item")
        Call<List<Item>> postItem(@Path("item") String item);

        @DELETE("item/id")
        Call<List<Item>> deleteItem(@Path("id") String itemID);
    }

    public ArrayList<Item> getItems(){
        IventoryService service = retrofit.create(IventoryService.class);
        final Call<List<Item>> items = service.getItems();
        items.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                for(Item item:response.body()) {
                    itemArrayList.add(item);
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Log.d("API", "error");
            }
        });

        return itemArrayList;
    }

}
