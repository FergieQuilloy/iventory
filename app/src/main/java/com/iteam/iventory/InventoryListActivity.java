package com.iteam.iventory;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.iteam.iventory.manager.API;
import com.iteam.iventory.models.Item;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InventoryListActivity extends AppCompatActivity {

    private static final int RC_OCR_CAPTURE = 9003;
    String[] itemArray = {"Octopus","Pig","Sheep","Rabbit","Snake","Spider" };

    String[] itemCountArray = {
            "1",
            "3",
            "3",
            "2",
            "4",
            "1"
    };


    ArrayList<Item> itemsArrayList = new ArrayList<Item>();
    ListView listView;
    CustomListAdapter whatever;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_list);

//        ArrayList<Item> itemList =  API.ourInstance.getItems();
//        itemsArrayList = itemList;
//        for(Item item:items) {
//            itemsArrayList.add(item);
//        }
       // Toast.makeText(this, items.toString(), Toast.LENGTH_LONG);

        whatever = new CustomListAdapter(this, itemsArrayList);
        listView = (ListView) findViewById(R.id.listviewID);
        listView.setAdapter(whatever);

        API.IventoryService service = API.ourInstance.retrofit.create(API.IventoryService.class);
        final Call<List<Item>> items = service.getItems();
        items.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                itemsArrayList.clear();
                whatever.clear();
                List<Item> body = response.body();
                for(Item item:body) {
                    itemsArrayList.add(item);
                }
                whatever.addAll(itemsArrayList);
                whatever.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Log.d("API", "error");
            }
        });


       FloatingActionButton cameraButton = findViewById(R.id.fab);
       cameraButton.setOnClickListener(
               new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       onCameraButtonClicked();
                   }
               }
       );

    }

    private void onCameraButtonClicked(){
        // launch Ocr capture activity.
        Intent intent = new Intent(this, OcrCaptureActivity.class);
        intent.putExtra(OcrCaptureActivity.AutoFocus, true);//autoFocus.isChecked());
        intent.putExtra(OcrCaptureActivity.UseFlash, false);//useFlash.isChecked());

        startActivityForResult(intent, RC_OCR_CAPTURE);
    }
}
