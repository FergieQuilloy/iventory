package com.iteam.iventory.models;

import com.google.gson.annotations.SerializedName;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by christinealcachupas on 25/11/2017.
 */

public class Item {
    @SerializedName("ID")
    private Integer id;

    @SerializedName("post_title")
    private String name;

    @SerializedName("menu_order")
    private Integer count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count.toString();
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
