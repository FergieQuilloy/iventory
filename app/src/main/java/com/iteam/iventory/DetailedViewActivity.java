package com.iteam.iventory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DetailedViewActivity extends AppCompatActivity {

    public static final String ItemName = "ItemName";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_view);
        EditText itemName = findViewById(R.id.editText);
        itemName.setText(getIntent().getStringExtra(ItemName));
        Button cancelButton = findViewById(R.id.button3);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        Button saveButton = findViewById(R.id.button1);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onClickSaveButton();
            }
        });
        Button deleteButton = findViewById(R.id.button2);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickDeleteButton();
            }
        });
    }
    private void onClickSaveButton (){
        Toast.makeText(this, getIntent().getStringExtra(ItemName) + " saved.", Toast.LENGTH_LONG).show();
    }
    private void onClickDeleteButton(){
        Toast.makeText(this,getIntent().getStringExtra(ItemName) + " deleted.", Toast.LENGTH_LONG).show();
    }
}
